document.addEventListener("DOMContentLoaded", () => {
	svg4everybody();

	var selects2, validation, swiper_base, swiper_baseSecondary, swiper_secondary, swiper_third, swiper_fourth, swiper_fourth_bg = $('[data-element="carousel-background"]');
	var animateCSS = function (element, animation, time, prefix = 'animate__') {
		return new Promise((resolve, reject) => {
			var animationName = `${prefix}${animation}`;
			var node = document.querySelector(element);

			node.classList.add(`${prefix}animated`, animationName);

			function handleAnimationEnd(event) {
				event.stopPropagation();
				setTimeout(function () {
					node.classList.remove(`${prefix}animated`, animationName);
					resolve('Animation ended');
				}, time)
			}

			node.addEventListener('animationend', handleAnimationEnd, { once: true });
		});
	};


	(function () {
		var selects = document.querySelectorAll('.form-select select');


		selects2 = Array.prototype.filter.call(selects, function (select) {
			var placeholder = $(select).data('placeholder') ?? 'Выберите значение...';
			var dropdownCssClass = $(select).data('containerClass') ?? '';
			var dropdownParent = $(select).parent();

			$(select).select2({
				placeholder,
				dropdownCssClass,
				dropdownParent,
				minimumResultsForSearch: Infinity
			})
		});
	})();


	(function () {
		var forms = document.querySelectorAll('.needs-validation');
		validation = Array.prototype.filter.call(forms, function (form) {

			form.addEventListener('submit', function (event) {
				if (form.checkValidity() === false) {
					event.preventDefault();
					event.stopPropagation();
				}
				form.classList.add('was-validated');
			}, false);
		});
	})();


	(function () {
		var menu = $('[data-element="menu"]');
		var sub_menu = $('[data-element="sub-menu"]');
		var links = $('[data-element="menu-link"]');

		links.each(function (i, el) {
			$(el).on('mouseover', function (e) {
				sub_menu.addClass('show')
				$(e.currentTarget).parent().siblings().removeClass('hovered')
				$(e.currentTarget).parent().addClass('hovered');


				$(`[data-menu-id="${$(el).data('target')}"]`).siblings('[data-menu-id]').attr('hidden', true)
				$(`[data-menu-id="${$(el).data('target')}"]`).removeAttr('hidden')
			})
		})

		menu.on('mouseleave', function (e) {
			// sub_menu.removeClass('show')
			links.each(function (i, el) {
				// $(el).parent().removeClass('hovered')
			})
		})

	})();



	(function () {

		var hamburger = $('[data-element="menu-hamburger"]');
		hamburger.on('click', function (e) {
			$(e.currentTarget).toggleClass('active');
			$(`[data-element=${$(e.currentTarget).data('target')}]`).toggleClass('show')
		})

	})();


	(function () {

		var collapseElements = $('.footer .collapse');
		var has_show_class = collapseElements.find('.show').prevObject.length;
		function setClass(node_list, condition) {
			if ($(window).width() < 1200 && condition) {
				node_list.each(function (i, el) {
					$(el).removeClass('show')
					$(el).prev().addClass('collapsed')
				})
			} else {
				node_list.each(function (i, el) {
					$(el).prev().removeClass('collapsed')
					$(el).addClass('show')
				})
			}
		}

		setClass(collapseElements, has_show_class)
		$(window).on('resize', function (e) {
			setClass(collapseElements, has_show_class)
		})

	})();



	(function () {

		swiper_base = new Swiper("[data-element='carousel-base']", {
			slidesPerView: "auto",
			spaceBetween: 30,
			loop: true,
			autoplay: {
				delay: 2500,
				disableOnInteraction: false,
			},
		});

		swiper_baseSecondary = new Swiper("[data-element='carousel-base-secondary']", {
			slidesPerView: "auto",
			spaceBetween: 30,
			loop: true,
			autoplay: {
				delay: 2500,
				disableOnInteraction: false,
			},
			navigation: {
				prevEl: ".section--services .swiper-button-prev",
			},
		});

		swiper_secondary = new Swiper("[data-element='carousel-secondary']", {
			slidesPerView: 4,
			spaceBetween: 30,
			loop: true,
			autoplay: {
				delay: 3000,
				disableOnInteraction: false,
			},
			navigation: {
				nextEl: ".swiper-button-next",
				prevEl: ".swiper-button-prev",
			},
			breakpoints: {
				0: {
					slidesPerView: 1,
				},
				768: {
					slidesPerView: 2,
					spaceBetween: 40,
				},
				1200: {
					slidesPerView: 3,
					spaceBetween: 50,
				},
				1920: {
					slidesPerView: 4,
					spaceBetween: 50,
				},
			},
		});

		swiper_secondary = new Swiper("[data-element='carousel-secondary-secondary']", {
			slidesPerView: 4,
			spaceBetween: 1,
			loop: true,
			autoplay: {
				delay: 3000,
				disableOnInteraction: false,
			},
			pagination: {
				el: ".swiper-pagination",
			},
			breakpoints: {
				0: {
					slidesPerView: 2,
				},
				768: {
					slidesPerView: 3,
					spaceBetween: 1,
				},
				1200: {
					slidesPerView: 4,
					spaceBetween: 1,
				},
			},
		});


		swiper_third = new Swiper("[data-element='carousel-third']", {
			slidesPerView: 1,
			loop: true,
			speed: 600,

			navigation: {
				nextEl: ".swiper-button-next",
				prevEl: ".swiper-button-prev",
			},
			effect: "flip",
			flipEffect: {
				slideShadows: false,
			},
			autoplay: {
				delay: 2500,
				disableOnInteraction: false,
			},
		});

		swiper_fourth = new Swiper("[data-element='carousel-fourth']", {
			slidesPerView: 1,
			speed: 600,
			effect: "fade",
			pagination: {
				el: ".swiper-pagination",
			},
			autoplay: {
				delay: 2500,
				disableOnInteraction: false,
			},
			on: {
				slideChange: function (swiper) {

					if ((swiper.$el).closest('.section--order')) {
						// 

						swiper_fourth_bg.css('backgroundImage', `url("${swiper.slides[swiper.activeIndex].dataset.bgSrc}")`)
					}
				}
			}
		});

	})();



	(function () {
		var img_container = $('[data-element="accordion-img-container"]');
		var title_container = $('[data-element="accordion-title-container"]');

		$('[data-element="accordion-img"]').on('show.bs.collapse', function (e) {
			var src = e.target.dataset.imgSrc;
			var title_content = e.target.dataset.title;
			img_container.attr('src', src);
			title_container.html(title_content);
			animateCSS('[data-element="accordion-img-container"]', 'fadeIn', 1000);
		})

	})();

	(function () {
		$('[data-element="tabs-base"] [data-toggle="tab"]').on('shown.bs.tab', function (event) {
			var tabs = $('[data-element="tabs-base"]');
			var pane = $($(event.target).attr('href'));
			var carousel_base = tabs.find('[data-element="carousel-base"]');
			// var carousels = [];
			carousel_base.each(function (i, el) {
				if (swiper_base.length) {
					var carouselIstance = swiper_base.find(function (instance) {
						return el === instance.$el[0]
					})
					carouselIstance.update()
				}
			})

		})


	})();


	(function () {
		$('[data-element="tabs-switch"]').on('change', function (e) {
			var parent = $(e.target).closest('[data-element="tabs-switch-parent"]');
			parent.find(`[data-checked="${!e.target.checked}"]`).removeClass('active')
			parent.find(`[data-checked="${e.target.checked}"]`).addClass('active')
			animateCSS(`[data-checked="${e.target.checked}"]`, 'fadeIn')
		})


	})();


	(function () {

		// $(":input")
		$("input").each(function (i, el) {
			if ($(el).attr('type') == 'tel') {
				$(el).inputmask();
				$(el).on('input', function (e) {


				})
			}

		})

	})();


	(function () {

		$('[data-element="select-tab"]').each(function (i, el) {
			$(el).on('select2:select', function (e) {
				$('[data-element="select-tab-parent"] [data-element="select-tab-pane"]').each(function (i, el) {
					$(el).removeClass('show')
				})
				$(`#${e.target.value}`).addClass('show')
				animateCSS(`#${e.target.value}`, 'fadeIn')
			})
		})

	})();


	(function () {
		var quiz = $('[data-element="quiz"]');
		var quizGroups = quiz.find('[data-element="quiz-group"]');
		var quizPrev = quiz.find('[data-element="quiz-prev"]');
		var quizNext = quiz.find('[data-element="quiz-next"]');
		var quizSubmit = quiz.find('[data-element="quiz-submit"]');
		var clearForms = function (parent, element) {
			parent.find(element).each(function (i, el) {
				el.checked = false;
			})
		};
		var setStep = function (e, groups, groupPaneSelector, siblingMethod) {
			e.preventDefault();
			groups.each(function (i, el) {
				var group = null;
				if ($(el).hasClass('show')) {
					group = $(el);
					var currentStep = null;
					var index = null;
					var nextStep = null;
					$(el).find(groupPaneSelector).each(function (i, paneEl) {
						if ($(paneEl).hasClass('show')) {
							currentStep = $(paneEl);
							nextStep = currentStep[siblingMethod]();
							index = siblingMethod == 'next' ? i + 1 : i - 1;
						}
					})
					$(currentStep).removeClass('show');
					nextStep.addClass('show');


					if (index == (group.find('[data-element="quiz-pane"]').length - 1)) {
						quizPrev.parent().addClass('d-block')
						quizNext.parent().addClass('d-none')
						quizSubmit.parent().addClass('d-block')

					} else if (index > 0 && index < (groups.length - 1)) {
						quizNext.parent().removeClass('d-none')
						quizPrev.parent().addClass('d-block')
						quizSubmit.parent().removeClass('d-block')

					} else if (index == 0) {
						quizPrev.parent().removeClass('d-block')

					}
				}
			})
		}
		$('[data-element="quiz-select"]').on('select2:select', function (e) {
			quizGroups.each(function (i, el) {
				clearForms(quiz, quiz.find('input[type="checkbox"]'));
				clearForms(quiz, quiz.find('input[type="radio"]'));
				$(el).removeClass('show');
				$(el).find('[data-element="quiz-pane"]').each(function (i, el) {
					$(el).removeClass('show');
				});
			})
			$(`#${e.target.value}`).addClass('show');
			animateCSS(`#${e.target.value}`, 'fadeIn');
			$($(`#${e.target.value}`).find('[data-element="quiz-pane"]')[0]).addClass('show');


			if ($(`#${e.target.value}`).find('[data-element="quiz-pane"]').length == 1) {

				quizSubmit.parent().addClass('d-block');
				quizPrev.parent().removeClass('d-block');
				quizNext.parent().addClass('d-none');
			} else if ($(`#${e.target.value}`).find('[data-element="quiz-pane"]').length > 1) {

				quizSubmit.parent().removeClass('d-block');
				quizPrev.parent().removeClass('d-block');
				quizNext.parent().removeClass('d-none');
			}
		});
		quizNext.on('click', function (e) {
			setStep(e, quizGroups, '[data-element="quiz-pane"]', 'next')
		})
		quizPrev.on('click', function (e) {
			setStep(e, quizGroups, '[data-element="quiz-pane"]', 'prev')
		})

	})();

});